import { Component } from "@angular/core";
import { map } from "rxjs/operators";
import { Breakpoints, BreakpointObserver } from "@angular/cdk/layout";

@Component({
  selector: "app-dashboard-estrategico",
  templateUrl: "./dashboard-estrategico.component.html",
  styleUrls: ["./dashboard-estrategico.component.css"]
})
export class DashboardEstrategicoComponent {
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: "Card 1", cols: 1, rows: 1 },
          { title: "Card 2", cols: 1, rows: 1 },
          { title: "Card 3", cols: 1, rows: 1 },
          { title: "Card 4", cols: 1, rows: 1 }
        ];
      }

      return [
        { title: "Card 1", cols: 1, rows: 1, descricao: "Alguma coisa" },
        { title: "Card 2", cols: 1, rows: 1, descricao: "uMA coisa" },
        { title: "Card 3", cols: 1, rows: 1, descricao: "DUAS coisa" },
        { title: "Card 4", cols: 1, rows: 1, descricao: "TRES coisa" }
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}
}
