import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FormularioComponent } from "./formulario/formulario.component";
import { DashboardOperacionalComponent } from "./dashboard-operacional/dashboard-operacional.component";
import { DashboardEstrategicoComponent } from "./dashboard-estrategico/dashboard-estrategico.component";
const routes: Routes = [
  { path: "formulario", component: FormularioComponent },
  { path: "dashboard-operacional", component: DashboardOperacionalComponent },
  { path: "dashboard-estrategico", component: DashboardEstrategicoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
